import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "searchNamePipe"
})
export class SearchNamePipePipe implements PipeTransform {
  transform(value: any[], searchQuarry: any): any {
    return searchQuarry
      ? value.filter(element => {
          return (
            String(element.firstName)
              .toLowerCase()
              .indexOf(searchQuarry.toLowerCase()) > -1
          );
        })
      : value;
  }
}
