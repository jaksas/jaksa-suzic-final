import { Component, OnInit } from "@angular/core";
import { ModelService } from "../../service/model.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-contacts",
  templateUrl: "./contacts.component.html"
})
export class ContactsComponent implements OnInit {
  constructor(
    public model: ModelService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  showSingleUser(id) {
    this.router.navigate(["/contact", id]);
  }

  editUser(id) {
    this.router.navigate(["/contact", id, "edit"]);
  }

  deleteUser(id) {
    this.model.deleteUser(id, () => {
      this.router.navigate(["/contacts"]);
    });
  }

  ngOnInit() {}
}
