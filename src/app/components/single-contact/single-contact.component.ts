import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-single-contact",
  templateUrl: "./single-contact.component.html"
})
export class SingleContactComponent implements OnInit {
  id;

  constructor(private route: ActivatedRoute) {
    this.route.paramMap.subscribe(objekat => {
      this.id = objekat.get("id");
    });
  }

  ngOnInit() {}
}
