import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ModelService } from "../../service/model.service";
import { ActivatedRoute, Router } from "@angular/router";
import { IMyDpOptions } from "../../../../node_modules/angular4-datepicker/src/my-date-picker";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html"
})
export class EditComponent implements OnInit {
  forma: FormGroup;
  user = {};
  id;
  formaSubmited = false;

  editDatePickerOptions: IMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };

  constructor(
    private model: ModelService,
    private route: ActivatedRoute,
    private ruter: Router
  ) {
    this.forma = new FormGroup({
      id: new FormControl({ value: "", disabled: true }),
      firstName: new FormControl({ value: "", disabled: true }),
      lastName: new FormControl({ value: "", disabled: true }),
      img: new FormControl(),
      age: new FormControl(),
      birthDate: new FormControl()
    });
  }

  submitForm() {
    this.formaSubmited = true;
    if (this.forma.valid) {
      let contact = {
        age: this.forma.value.age,
        birthDate: this.forma.value.birthDate.jsdate,
        img: this.forma.value.img,
        id: this.id
      };
      //console.log(contact);
      //this.forma.getRawValue()
      this.model.updateUser(contact, () => {
        this.ruter.navigate(["/contacts"]);
      });
    }
  }
  ngOnInit() {
    this.route.parent.paramMap.subscribe(params => {
      this.model.getSingleUser(params.get("id"), obj => {
        this.id = params.get("id");
        this.user = obj;
        var date = new Date(obj.birthDate);
        //console.log(date, obj.birthDate);
        let initialValues = {
          ...this.user,
          birthDate: {
            date: {
              day: date.getDate(),
              month: date.getMonth() + 1,
              year: date.getFullYear()
            }
          }
        };
        //console.log(initialValues);
        // delete initialValues['id'];
        // this.forma.setValue(initialValues)
        this.forma.setValue(initialValues);
      });
    });
  }
}
