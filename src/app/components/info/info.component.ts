import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ModelService } from "../../service/model.service";

@Component({
  selector: "app-info",
  templateUrl: "./info.component.html"
})
export class InfoComponent implements OnInit {
  id;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private model: ModelService
  ) {}

  user = {};

  ngOnInit() {
    this.route.parent.paramMap.subscribe(params => {
      this.model.getSingleUser(params.get("id"), objekat => {
        this.user = objekat;
      });
    });
  }
}
