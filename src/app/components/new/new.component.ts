import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ModelService } from "../../service/model.service";
import { ActivatedRoute, Router } from "@angular/router";
import { IMyDpOptions } from "../../../../node_modules/angular4-datepicker/src/my-date-picker";

@Component({
  selector: "app-new",
  templateUrl: "./new.component.html"
})
export class NewComponent implements OnInit {
  forma: FormGroup;
  trenutnaGodina;
  godinaRodjenja;
  starost;
  formaSubmited;
  editDatePickerOptions: IMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };

  constructor(
    private model: ModelService,
    private route: ActivatedRoute,
    private ruter: Router
  ) {
    this.formaSubmited = false;
    this.forma = new FormGroup({
      id: new FormControl(),
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      img: new FormControl("", Validators.required),
      age: new FormControl(""),
      birthDate: new FormControl("")
    });
  }

  submitForm() {
    this.formaSubmited = true;
    console.log(this.forma.valid, this.formaSubmited);
    if (this.forma.valid) {
      // console.log(
      //   "Trenutno je godina: " + this.trenutnaGodina,
      //   "godina rodjenja je: " + this.godinaRodjenja,
      //   "starost je: " + this.starost
      // );
      if (
        this.forma.controls.age.value &&
        this.forma.controls.birthDate.value
      ) {
        this.trenutnaGodina = new Date().getFullYear();
        this.godinaRodjenja = this.forma.value.birthDate.jsdate.getFullYear();
        this.starost = this.forma.controls.age.value;
        console.log("Prvi if");
        if (this.trenutnaGodina - this.godinaRodjenja == this.starost) {
          let contact = {
            firstName: this.forma.value.firstName,
            lastName: this.forma.value.lastName,
            age: this.forma.value.age,
            birthDate: this.forma.value.birthDate.jsdate,
            img: this.forma.value.img,
            id: this.forma.value.id
          };
          delete contact["id"];
          //console.log(contact);
          this.model.addNewUser(contact, () => {
            this.ruter.navigate(["/contacts"]);
          });
        } else {
          alert("Starost nije tacna.");
        }
      } else if (
        this.forma.controls.age.value ||
        this.forma.controls.birthDate.value
      ) {
        console.log("Drugi if");
        let contact = {
          firstName: this.forma.value.firstName,
          lastName: this.forma.value.lastName,
          age: this.forma.value.age,
          birthDate: this.forma.value.birthDate.jsdate,
          img: this.forma.value.img,
          id: this.forma.value.id
        };
        delete contact["id"];
        console.log(contact);
        this.model.addNewUser(contact, () => {
          this.ruter.navigate(["/contacts"]);
        });
      } else {
        alert("Morate uneti starost ili datum rodjenja(ili oba)");
      }
    }
  }

  ngOnInit() {}
}
